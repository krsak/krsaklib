﻿using System;
using System.Numerics;

namespace Krsak.Lib.Prefix
{
	/// <summary>
	/// 2進接頭辞っぽいものを付けられる拡張メソッド
	/// </summary>
	/// <example>
	/// <code>
	/// var filesize = 50.KiB();
	/// </code>
	/// </example>
	public static class BinaryPrefix
	{
		public const int Kibi = 1024;
		public const int Mebi = 1024 * BinaryPrefix.Kibi;
		public const long Gibi = 1024 * BinaryPrefix.Mebi;
		public const long Tebi = 1024 * BinaryPrefix.Gibi;
		public const long Pebi = 1024 * BinaryPrefix.Tebi;
		public static BigInteger Exbi = 1024 * BinaryPrefix.Pebi;
		public static BigInteger Zebi = 1024 * BinaryPrefix.Exbi;
		public static BigInteger Yobi = 1024 * BinaryPrefix.Zebi;

		public static int KiB(this int value) { return value * BinaryPrefix.Kibi; }
		public static int MiB(this int value) { return value * BinaryPrefix.Mebi; }
		public static long GiB(this int value) { return value * BinaryPrefix.Gibi; }
		public static long TiB(this int value) { return value * BinaryPrefix.Tebi; }
		public static long PiB(this int value) { return value * BinaryPrefix.Pebi; }
		public static BigInteger EiB(this int value) { return value * BinaryPrefix.Exbi; }
		public static BigInteger ZiB(this int value) { return value * BinaryPrefix.Zebi; }
		public static BigInteger YiB(this int value) { return value * BinaryPrefix.Yobi; }
	}
}

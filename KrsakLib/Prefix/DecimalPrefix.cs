﻿using System;
using System.Numerics;

namespace Krsak.Lib.Prefix
{
	/// <summary>
	/// 10進接頭辞っぽいものを付けられる拡張メソッド
	/// </summary>
	/// <example>
	/// <code>
	/// var filesize = 50.KB();
	/// </code>
	/// </example>
	public static class DecimalPrefix
	{
		public const int Kilo = 1000;
		public const int Mega = 1000 * DecimalPrefix.Kilo;
		public const long Giga = 1000 * DecimalPrefix.Mega;
		public const long Tera = 1000 * DecimalPrefix.Giga;
		public const long Peta = 1000 * DecimalPrefix.Tera;
		public static BigInteger Exa = 1000 * DecimalPrefix.Peta;
		public static BigInteger Zetta = 1000 * DecimalPrefix.Exa;
		public static BigInteger Yotta = 1000 * DecimalPrefix.Zetta;

		public static int KB(this int value) { return value * DecimalPrefix.Kilo; }
		public static int MB(this int value) { return value * DecimalPrefix.Mega; }
		public static long GB(this int value) { return value * DecimalPrefix.Giga; }
		public static long TB(this int value) { return value * DecimalPrefix.Tera; }
		public static long PB(this int value) { return value * DecimalPrefix.Peta; }
		public static BigInteger EB(this int value) { return value * DecimalPrefix.Exa; }
		public static BigInteger ZB(this int value) { return value * DecimalPrefix.Zetta; }
		public static BigInteger YB(this int value) { return value * DecimalPrefix.Yotta; }
	}
}
